# Arkanoid

**Arkanoid** est un jeu de casse-brique gratuit dont le but du jeu est de briser toutes les briques du niveau pour gagner la partie.



## Installation

**1**: Télécharger le dossier compressé

**2**: Décompresser le dossier

**3**: Télécharger Greenfoot

**4**: Ouvrir le dossier décompressé avec Greenfoot

**5**: Appuyez sur Run pour pouvoir jouer au jeu



## Comment Jouer

**Règles**: Les règles son simple, utiliser moins de 3 balles afin de casser tout les briques

**Touches**: Utiliser les flèches du clavier afin de ce déplacer de gauche à droite.
La touche espace est utilisé afin de lancer une balle.

**Général**: Le but est de cassé l'ensemble des brique présente dans le monde afin de passer au niveau suivant. Les briques ce cassent au contacte de la balle.



## Version

* 1.0.0
  * Première version



## Meta

* **Auteur**:
  * Christopher Caron
  * Evelyne Jutras
  * Félix Giard
  * William Beaudoin
* **Date**: 
  * 23 avril 2020



## Licence

* Distribué sous la licence MIT. Veuillez-vous référer au fichier ``LICENCE.txt`` pour plus d'information.



## Contribuer

**1**: Fork le projet (https://gitlab.com/Amatsu-Kami/tp1_outils/-/forks)

**2**: Créer une Branch

**3**: Commit les changements

**4**: Push sur la Branch

**5**: Créer un nouveau Pull request