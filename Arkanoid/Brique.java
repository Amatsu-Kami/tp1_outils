/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Représente une brique dans le jeu Arkanoid.
 * 
 * @author Christopher Caron 
 * @version 1.0
 * @since 14 avril 2020
 */
public class Brique extends Actor
{
    private double deltaX;
    private double deltaY;
    private int rayon;
    /**
     * Constructeur permettant de spécifier la couleur de la brique ainsi que la grosseur de celle-ci.
     * 
     * @param couleur La couleur de la brique.
     */
    public Brique(Color couleur){
     GreenfootImage image = new GreenfootImage(50, 20);
     image.setColor(couleur);
     image.fill();
     setImage(image);
    }
    /**
     * Permet de retourner le monde Arkanoid.
     * 
     * @return Retourne le monde Arkanoid.
     */
    private Arkanoid monde(){
      return (Arkanoid) getWorld();
    }
}
