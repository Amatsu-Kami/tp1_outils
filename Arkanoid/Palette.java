/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Représente la palette du jeu Arkanoid.
 * 
 * @author  Christopher Caron
 * @version 1.0
 * @since 14 avril 2020
 */
public class Palette extends Actor
{
    public Balle balle1;
    public Balle balle2;
    private String toucheGauche;
    private String toucheDroite;
    private static final int VITESSE = 9;
    /**
     * quand la palette se crée, une balle se crée aussi.
     *
     * @param world Le monde du jeu
     */
    public void addedToWorld(World world){
        nouvelleBalle();
    }
    /**
     * Constructeur permettant de spécifier la couleur, la dimension et les directions de la palette.
     * 
     * @param couleur La couleur de la palette.
     * @param toucheGauche La touche pour se déplacer vers la gauche.
     * @param toucheDroite La touche pour se déplacer vers la droite.
     */
    public Palette(Color couleur, String toucheGauche, String toucheDroite){
     GreenfootImage image = new GreenfootImage(200, 20);
     image.setColor(couleur);
     image.fill();
     setImage(image);
     
     this.toucheGauche = toucheGauche;
     this.toucheDroite = toucheDroite;
    }
    /**
     * Permet d'ajouter une nouvelle balle.
     */
    public void nouvelleBalle(){
        balle1 = new Balle (1,1);
        getWorld().addObject(balle1, getX(), getY()-25);
    }
    /**
     * Permet de gérer la direction de la palette et permet que la balle reste collé sur la palette au début.
     */
    public void act() {
        if(!monde().getPause()){
        //direction de la palette et la vitesse
        int direction = 0;
        if (Greenfoot.isKeyDown(toucheGauche)){
         direction = -1;
        }else if(Greenfoot.isKeyDown(toucheDroite)){
            direction = 1;
        }
        balleDebut();
        //Fait en sorte que la balle reste collé sur la palette au début du jeu
        if( balle1 != null){
            balle1.setLocation(getX()+ direction * VITESSE,getY()-25);
        }
        setLocation(getX()+ direction * VITESSE,getY());
       }
    }
    /**
     * Permet de tirer la balle qui est collé sur la palette.
     */
    private void balleDebut(){
        if (balle1 != null &&  Greenfoot.isKeyDown ("space")) {
            debutPartieBalle();
        }
    }
    /**
     * Permet de transformer la balle1 en balle2 une fois que celle-ci est tiré.
     */
    public void debutPartieBalle(){
        balle1.debutPartie();
        balle2 = balle1;
        balle1 = null;
    }
    /**
     * Permet de retourner le monde Arkanoid.
     * 
     * @return Retourne le monde Arkanoid.
     */
    private Arkanoid monde(){
        return (Arkanoid) getWorld();
    }
}
