import com.sun.javafx.application.PlatformImpl;
import greenfoot.core.Simulation;
import greenfoot.core.WorldHandler;
import greenfoot.export.GreenfootScenarioApplication;
import greenfoot.gui.input.mouse.MousePollingManager;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;

abstract class GreenfootTest {
    @BeforeAll
    static void beforeAll() throws InterruptedException {
        PlatformImpl.setImplicitExit(false);
        final CountDownLatch latch = new CountDownLatch(1);
        PlatformImpl.startup(() ->
        {
            try {
                new GreenfootScenarioApplication().start(new Stage());
                latch.countDown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        latch.await();
    }

    @AfterAll
    static void afterAll() {
        Simulation.getInstance().sleep(1);
        PlatformImpl.setImplicitExit(true);
    }

    void act() throws InterruptedException {
        var sim = Simulation.getInstance();
        sim.runOnce();
        sim.sleep(1);
        Thread.sleep(1000);
    }

    void keyPressed(KeyCode keyCode) {
        WorldHandler.getInstance().getKeyboardManager().keyPressed(keyCode, keyCode.getName().toLowerCase());
    }

    void keyReleased(KeyCode keyCode) {
        WorldHandler.getInstance().getKeyboardManager().keyReleased(keyCode, keyCode.getName().toLowerCase());
    }

    void keyTyped(KeyCode keyCode) {
        WorldHandler.getInstance().getKeyboardManager().keyTyped(keyCode, keyCode.getName().toLowerCase());
    }

    void mouseClicked(int x, int y) {
        WorldHandler.getInstance().getMouseManager().mouseClicked(x, y, MouseButton.PRIMARY, 1);
    }

    void mouseClicked(int x, int y, MouseButton mouseButton) {
        WorldHandler.getInstance().getMouseManager().mouseClicked(x, y, mouseButton, 1);
    }

    void mouseClicked(int x, int y, MouseButton mouseButton, int count) {
        WorldHandler.getInstance().getMouseManager().mouseClicked(x, y, mouseButton, count);
    }

    void mousePressed(int x, int y) {
        WorldHandler.getInstance().getMouseManager().mousePressed(x, y, MouseButton.PRIMARY);
    }

    void mousePressed(int x, int y, MouseButton mouseButton) {
        WorldHandler.getInstance().getMouseManager().mousePressed(x, y, mouseButton);
    }

    void mouseReleased(int x, int y) {
        WorldHandler.getInstance().getMouseManager().mouseReleased(x, y, MouseButton.PRIMARY);
    }

    void mouseReleased(int x, int y, MouseButton mouseButton) {
        WorldHandler.getInstance().getMouseManager().mouseReleased(x, y, mouseButton);
    }
}
