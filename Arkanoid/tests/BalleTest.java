import greenfoot.*;
import javafx.scene.input.KeyCode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BalleTest extends GreenfootTest {
    @Test
    void balleColleAuDebutDuJeu_DeplacementPaletteGauche() throws InterruptedException {

        World world = new Arkanoid();
        Greenfoot.setWorld(world);
        Balle balle = world.getObjects(Balle.class).get(0);

        assertEquals(400, balle.getX());
        assertEquals(555, balle.getY());

        keyPressed(KeyCode.LEFT);
        act();

        assertEquals(391, balle.getX());
        assertEquals(555, balle.getY());

        keyReleased(KeyCode.LEFT);
        act();

        assertEquals(391, balle.getX());
        assertEquals(555, balle.getY());
    }

    @Test
    void balleColleAuDebutDuJeu_DeplacementPaletteDroite() throws InterruptedException {

        World world = new Arkanoid();
        Greenfoot.setWorld(world);
        Balle balle = world.getObjects(Balle.class).get(0);

        assertEquals(400, balle.getX());
        assertEquals(555, balle.getY());

        keyPressed(KeyCode.RIGHT);
        act();

        assertEquals(409, balle.getX());
        assertEquals(555, balle.getY());

        keyReleased(KeyCode.RIGHT);
        act();

        assertEquals(409, balle.getX());
        assertEquals(555, balle.getY());
    }

    @Test
    void balleDecolleAuDebutDuJeu_DeplacementPaletteGauche() throws InterruptedException {

        World world = new Arkanoid();
        Greenfoot.setWorld(world);
        Balle balle = world.getObjects(Balle.class).get(0);

        assertEquals(400, balle.getX());
        assertEquals(555, balle.getY());

        keyPressed(KeyCode.SPACE);
        act();

        assertEquals(391, balle.getX());
        assertEquals(546, balle.getY());

        keyReleased(KeyCode.SPACE);
        act();

        assertEquals(382, balle.getX());
        assertEquals(537, balle.getY());
    }

}