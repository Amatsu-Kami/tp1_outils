import greenfoot.*;
import javafx.scene.input.KeyCode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaletteTest extends GreenfootTest {
    @Test
    void deplacementPaletteGauche() throws InterruptedException {

        World world = new Arkanoid();
        Greenfoot.setWorld(world);
        Palette palette = world.getObjects(Palette.class).get(0);

        assertEquals(400, palette.getX());
        assertEquals(580, palette.getY());

        keyPressed(KeyCode.LEFT);
        act();

        assertEquals(391, palette.getX());
        assertEquals(580, palette.getY());

        keyReleased(KeyCode.LEFT);
        act();

        assertEquals(391, palette.getX());
        assertEquals(580, palette.getY());
    }

    @Test
    void deplacementPaletteDroite() throws InterruptedException {

        World world = new Arkanoid();
        Greenfoot.setWorld(world);
        Palette palette = world.getObjects(Palette.class).get(0);

        assertEquals(400, palette.getX());
        assertEquals(580, palette.getY());

        keyPressed(KeyCode.RIGHT);
        act();

        assertEquals(409, palette.getX());
        assertEquals(580, palette.getY());

        keyReleased(KeyCode.RIGHT);
        act();

        assertEquals(409, palette.getX());
        assertEquals(580, palette.getY());
    }

}