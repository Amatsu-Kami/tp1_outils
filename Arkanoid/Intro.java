/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Représente l'écran d'introduction. Les instructions sont indiqué sur cet écran.
 * 
 * @author Christopher Caron 
 * @version 1.0
 * @since 14 avril 2020
 */
public class Intro extends World
{
    /**
     * Constructeur d'Intro permettant d'afficher les règlements.
     * 
     */
    public Intro()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1);
        
        Affichage txt;
        txt = new Affichage("Arkanoid",80);
        addObject(txt,400,100);
        
        txt = new Affichage("Veuillez utiliser les flêches pour vous déplacer de droite à gauche", 30);
        addObject (txt,400,200);
        
        txt = new Affichage("Vous possédez 3 balles au total", 30);
        addObject (txt, 400,260);
        
        txt = new Affichage("Appuyer sur la touche escape pour mettre le jeu en pause", 30);
        addObject (txt, 400,320);
        
        txt = new Affichage("Appuyer sur la touche d'espacement pour lancer la balle", 30);
        addObject (txt, 400,390);
        
        txt = new Affichage("Appuyer sur la touche enter pour démarrer la partie", 30);
        addObject (txt, 400,450);
    }
    /**
     * Permet d'accéder à l'écran du jeu.
     */
    public void act() {
        String touche = Greenfoot.getKey();
        if ( touche != null  && touche.equals("enter")){
            Arkanoid arkanoid = new Arkanoid();
            Greenfoot.setWorld(arkanoid);
        }
        
    }
}
