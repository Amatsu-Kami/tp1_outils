/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Affichage d'un texte dans une image
 * 
 * @author Christopher Caron
 * @version 1.0
 * @since 14 avril 2020
 */
public class Affichage extends Actor
{
    /**
     * Constructeur permettant de spécifier le texte à afficher ainsi que la taille de ce texte
     * <p>
     * La couleur du texte par défaut est noire et l'arrière plan est transparent
     * </p>
     * @param txt Le texte à afficher
     * @param taille La taille du texte
     */
    public Affichage(String txt, int taille) {
        this(txt, taille, Color.BLACK, new Color (0,0,0,0));
        
    }
    /**
     * Constructeur permettant de spécifier le texte à afficher, la taille de ce texte, la couleur de ce texte et la couleur de l'arrière plan
     * 
     * @param txt Le texte à afficher
     * @param taille La taille du texte
     * @param couleurTxt La couleur du texte
     * @param couleurFond La couleur d'arriêre plan
     */
    public Affichage(String txt, int taille, Color couleurTxt, Color couleurFond) {
        GreenfootImage image = new GreenfootImage(txt, taille , couleurTxt, couleurFond);
        setImage(image);
    }
}