/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Représente l'écran pause.
 * 
 * @author Christopher Caron
 * @version 1.0
 * @since 14 avril 2020
 */
public class ecranPause extends Actor
{
    /**
     * Quand le monde se crée, l'écran pause se crée aussi.
     *
     * @param world le monde du jeu
     */
    public void addedToWorld(World world){
        ecranPause();
    }
    /**
     * Permet d'enlever l'écran pause si une certaine touche est appuyé.
     */
    public void act() 
    {
       String touche = Greenfoot.getKey();
       if (touche != null && touche.equals("escape")){
         getWorld().removeObject(this);
        }
    }
    /**
     * Permet de spécifier les dimensions de l'image, la position et la transparence de celle-ci.
     */
    public void ecranPause(){
        GreenfootImage image = new GreenfootImage(20, 200);
        getImage().scale(800,600);
        getImage().setTransparency(150);
    }
}
