/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Représente le pointage du jeu.
 * 
 * @author Christopher Caron
 * @version 1.0
 * @since 14 avril 2020
 */
public class Pointage extends Actor
{
    int point;
    /**
     * Permet d'afficher le pointage au début.
     */
    public Pointage(){
        point = 0;
        affiche();
    }
    /**
     * Permet d'afficher le pointage et d'ajouter un point quand une brique est touché.
     */
    public void score(){
        point++;
        affiche();
    }
    /**
     * Permet de créer une image et de mettre le pointage à l'intérieur et permet de choisir la couleur.
     */
    private void affiche() {
        GreenfootImage image = new GreenfootImage(100,20);
        image.setColor(Color.GREEN);
        image.fill();
        image.setColor(Color.RED);
        image.drawString(String.valueOf(point), image.getWidth()/2, 15);
        setImage(image);
    }
}
