package threadchecker;

public enum Tag {
    FXPlatform,
    FX_WaitsForSwing,
    SwingPlatform,
    Swing_WaitsForFX,
    Unique,
    Simulation,
    Any;

    private Tag() {
    }

    public boolean canOverride(Tag var1) {
        if (var1 == null) {
            return this == Any;
        } else if (var1 == FX_WaitsForSwing && this == FXPlatform) {
            return true;
        } else if (var1 == Swing_WaitsForFX && this == SwingPlatform) {
            return true;
        } else {
            return this == var1;
        }
    }

    public boolean canCall(Tag var1, boolean var2) {
        if (var1 != null && var1 != Any) {
            if (var1 != Unique) {
                if (var1 == FXPlatform && this == FX_WaitsForSwing) {
                    return true;
                } else if (var1 == SwingPlatform && this == Swing_WaitsForFX) {
                    return true;
                } else {
                    return this == var1;
                }
            } else {
                return var2 && this == Unique;
            }
        } else {
            return true;
        }
    }
}