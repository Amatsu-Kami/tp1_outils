/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Représente une balle dans le jeu Arkanoid.
 * 
 * @author Christopher Caron
 * @version 1.0
 * @since 14 avril 2020
 */
public class Balle extends Actor
{
    private double deltaX;
    private double deltaY;
    private boolean coller = true;
    private int rayon;
    private String toucheGauche;
    private String toucheDroite;
    private Pointage pointage;
    
    private static final int VITESSE = 9;
    /**
     * Permet les déplacement de la balle et ses paramètres.
     */
    public void act() 
    {
      if(!monde().getPause()){
       if(!coller){
       setLocation(getX() + (int)(deltaX * VITESSE), getY() + (int)(deltaY * VITESSE));
      }
      
      balleCotes();
      
      balleHaut();
      
      ballePalette();
      
      balleBrique();
      
      verifierHorsJeu();
     }
    }
    /**
     * Permet de détecter si la balle frappe une brique et retire celle-ci si c'est le cas, puis renvoie celle-ci s'il y a un contact.
     * <p>
     * Ajoute un point à chaque fois qu'une brique est touché.
     * </p>
     */
    private void balleBrique(){
      Actor brique = getOneIntersectingObject(Brique.class);
      if(brique != null){
         gererCollisionBrique((Brique) brique);
         getWorld().removeObject(brique);
         monde().balleScore();
         Greenfoot.playSound("blip.wav");
        }
    }
    /**
     * Permet de détecter si la balle frappe la palette et de la renvoyer si c'est le cas.
     */
    private void ballePalette(){
      Actor palette = getOneIntersectingObject(Palette.class);
      if(palette != null){
         gererCollisionPalette((Palette) palette);
         Greenfoot.playSound("boop.wav");
        }
    }
    /**
     * Permet de détecter si la balle frappe le haut du jeu et permet de la renvoyer si c'est le cas.
     */
    private void balleHaut(){
        //delta < 0 sert à empêcher que la balle reste coincée sur la bordure
      if (getY() - rayon <= 0 && deltaY < 0 ){
          deltaY = -deltaY;
        }
    }
    /**
     * Permet de détecter si la balle frappe les cotés du jeu et permet de la renvoyer si c'est le cas.
     */
    private void balleCotes(){
      //delta < 0 sert à empêcher que la balle reste coincée sur la bordure
        if (getX() - rayon <= 0 && deltaX < 0 || getX() + rayon >= getWorld().getWidth() && deltaX > 0) {
          deltaX = -deltaX;
      }
    }
    /**
     * Permet de lancer la balle au début.
     */
    public void debutPartie(){
        deltaX = -1;
        deltaY = -1;
        coller = false;
    }
    /**
     * Permet de renvoyer la balle losrqu'il y a une collision avec la palette.
     * 
     * @param palette La palette du jeu.
     */
    private void gererCollisionPalette(Palette palette){
     deltaY = -deltaY/Math.abs(deltaY);
     deltaX = (getX()-palette.getX())/(palette.getImage().getWidth()/2.0);
     normaliser();
    }
    /**
     * Permet de renvoyer la balle lorsqu'il y a une collision avec une brique.
     * 
     * @param brique Une des brique du jeu.
     */
    private void gererCollisionBrique(Brique brique){
     deltaY = -deltaY;
     normaliser();
    }
    /**
     * Constructeur permettant 2 déplacement.
     * 
     * @param deltaX Déplacement en x.
     * @param deltaY Déplacement en y.
     */  
    public Balle(double deltaX, double deltaY){
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.rayon = getImage().getWidth()/2;
        normaliser();
    }
    /**
     * Assure que la vitesse de la balle sera constante peu importe les delta
     */
    private void normaliser() {
        double hypotenuse = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        deltaX = deltaX / hypotenuse;
        deltaY = deltaY / hypotenuse;
    }
    /**
     * Permet de vérifier si la balle touche le bas de l'écran et de la retirer si c'est le cas.
     */
    public void verifierHorsJeu(){
      if (getY() + rayon >= getWorld().getHeight() && deltaY > 0){
          monde().balleHorsJeu();
          getWorld().removeObject(this);
          Greenfoot.playSound("lost.wav");
        }
    }
    /**
     * Permet de retourner le monde Arkanoid.
     * 
     * @return Retourne le monde Arkanoid.
     */
    private Arkanoid monde(){
      return (Arkanoid) getWorld();
    }
}
