/**
 * Copyright (C) 2020 by Christopher Caron, Félix Giard, Évelyne Jutras et William Beaudoin
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Représente le monde du jeu Arkanoid.
 * 
 * @author Christopher Caron
 * @version 1.0
 * @since 14 avril 2020
 */
public class Arkanoid extends World
{
    public static Palette palette;
    public static Balle balle;
    private Brique[] briqueArray;
    public Pointage pointage;
    private ecranPause stop;
    public int vie = 3;
    private Vie vie1 = new Vie();
    private Vie vie2 = new Vie();
    private Vie vie3 = new Vie();
    private boolean pause = false;
    /**
     * Constructeur d'Arkanoid permettant de creer une palette et  les briques, afficher les vies et afficher le pointage.
     * 
     */
    public Arkanoid()
    {    
        // Create a new world with 800x600 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        
        
        palette = new Palette(Color.RED, "left", "right");
        addObject(palette, palette.getImage().getWidth()*2, getHeight()-20);

        //pour ajouter des brique
        briqueArray = new Brique [14];
        for (int i = 0; i< 14; i++)
        {
            addObject(new Brique(Color.YELLOW), (42+(55*i)), 50);
            addObject(new Brique(Color.YELLOW), (42+(55*i)), 80);
            addObject(new Brique(Color.YELLOW), (42+(55*i)), 110);
            addObject(new Brique(Color.YELLOW), (42+(55*i)), 140);
            addObject(new Brique(Color.YELLOW), (42+(55*i)), 170);
        }
        
        //sert à ajouter les vies
        addObject( vie1, 25, 580);
        addObject( vie2, 55, 580);
        addObject( vie3, 85, 580);
        
        pointage = new Pointage();
        addObject(pointage, getWidth()/2,10);
    }
    /**
     * Permet de mettre le jeu en pause, de verifier si la partie est gagnée et vérifie le nombre de vies.
     */
    public void act(){
        mettreEcranPause();
        
        verifierVictoire();
        
        verifierVie();
    }
    /**
     * Permet de vérifier si la partie est gagnée.
     */
    private void verifierVictoire(){
        if(getObjects(Brique.class).isEmpty()){
            ecranFin fin = new ecranFin();
            Greenfoot.setWorld(fin);
            fin.addObject(pointage,500,260);
        }
    }
    /**
     * Permet de mettre le jeu en pause.
     */
    private void mettreEcranPause(){
        String touche = Greenfoot.getKey();
        if (pause == false && touche != null && touche.equals("escape")){
         pause = true;
         stop = new ecranPause();
         addObject(stop, stop.getImage().getWidth()/2, getHeight()/2);
        } else if(pause == true && touche != null && touche.equals("escape")){
         removeObject(stop);
         pause = false;
        }
    }
    /**
     * Permet d'ajouter un point au pointage.
     */
    public void balleScore() {
        pointage.score();
    }
    /**
     * Permet d'ajouter une balle dans le jeu et enlève une balle quand une nouvelle balle est ajoutée.
     */
    public void balleHorsJeu(){
        palette.nouvelleBalle();
        vie--;
    }
    /**
     * Permet de vérifier le nombre de vie et d'afficher l'écran de fin s'il n'y a pu de vie.
     */
    private void verifierVie(){
        if(vie == 2){
            removeObject(vie3);
        }
        if(vie == 1){
            removeObject(vie2);
        }
        if(vie == 0){
            removeObject(vie1);
            ecranFin fin = new ecranFin();
            Greenfoot.setWorld(fin);
            fin.addObject(pointage,500,260);
        }
    }
    /**
     * Permet de savoir si le jeu est en pause.
     * 
     * @return Retourne vrai si le jeu est en pause. Sinon, retourne faux.
     */
    public boolean getPause(){
        return pause;
    }
}
